namespace TheCoffeeShop.Contracts
{
    using System;


    public interface IOrderRejected
    {
        Guid OrderId { get; }
        DateTime Timestamp { get; }

        string Reason { get; }
    }
}