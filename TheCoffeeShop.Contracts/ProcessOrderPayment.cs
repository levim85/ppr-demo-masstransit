namespace TheCoffeeShop.Contracts
{
    using System;
    using Models;


    public interface IProcessOrderPayment
    {
        Guid CommandId { get; }

        IOrder Order { get; }
    }
}