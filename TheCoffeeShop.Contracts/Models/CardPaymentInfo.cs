namespace TheCoffeeShop.Contracts.Models
{
    public interface ICardPaymentInfo
    {
        string VaultTokenId { get; }
        string PostalCode { get; }
        string SecurityCode { get; }
    }
}