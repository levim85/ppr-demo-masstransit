namespace TheCoffeeShop.Contracts.Models
{
    public interface IPaymentReceipt
    {
        string Source { get; }
        decimal Amount { get; }
    }
}