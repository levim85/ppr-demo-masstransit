namespace TheCoffeeShop.Contracts.Models
{
    using System;


    public interface IOrder
    {
        Guid OrderId { get; }

        IOrderLine[] Lines { get; }
    }
}