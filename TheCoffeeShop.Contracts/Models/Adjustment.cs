namespace TheCoffeeShop.Contracts.Models
{
    using System;


    public interface IAdjustment
    {
        Guid AdjustmentId { get; }

        IAdjustmentLine[] AdjustmentLines { get; }
    }
}