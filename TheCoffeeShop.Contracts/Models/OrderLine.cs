namespace TheCoffeeShop.Contracts.Models
{
    using System;


    public interface IOrderLine
    {
        Guid OrderLineId { get; }
    }
}