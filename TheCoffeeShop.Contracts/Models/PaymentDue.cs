namespace TheCoffeeShop.Contracts.Models
{
    public interface IPaymentDue
    {
        decimal Amount { get; }
    }
}