namespace TheCoffeeShop.Contracts
{
    using System;


    public interface IOrderAccepted
    {
        Guid OrderId { get; }

        DateTime Timestamp { get; }
    }
}