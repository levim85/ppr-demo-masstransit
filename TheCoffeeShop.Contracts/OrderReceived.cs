namespace TheCoffeeShop.Contracts
{
    using System;
    using Models;


    public interface IOrderReceived
    {
        Guid OrderId { get; }

        DateTime Timestamp { get; }

        IOrder Order { get; }
    }
}