﻿namespace TheCoffeeShop.Contracts
{
    using System;


    public interface ISubmitOrder
    {
        Guid OrderId { get; }

        DateTime Timestamp { get; }
    }
}