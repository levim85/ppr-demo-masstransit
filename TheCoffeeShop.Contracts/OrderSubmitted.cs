namespace TheCoffeeShop.Contracts
{
    using System;


    public interface IOrderSubmitted
    {
        Guid OrderId { get; }
        DateTime Timestamp { get; }
    }
}