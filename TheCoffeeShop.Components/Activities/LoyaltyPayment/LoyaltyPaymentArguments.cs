namespace TheCoffeeShop.Components.Activities.LoyaltyPayment
{
    using Contracts.Models;


    public interface LoyaltyPaymentArguments
    {
        IOrder Order { get; }

        IAdjustment[] Adjustments { get; }

        IPaymentDue PaymentDue { get; }
    }
}