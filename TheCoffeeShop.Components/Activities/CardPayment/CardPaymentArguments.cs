namespace TheCoffeeShop.Components.Activities.CardPayment
{
    using Contracts.Models;


    public interface CardPaymentArguments
    {
        IOrder Order { get; }

        ICardPaymentInfo PaymentInfo { get; }

        IAdjustment[] Adjustments { get; }

        IPaymentDue PaymentDue { get; }
    }
}