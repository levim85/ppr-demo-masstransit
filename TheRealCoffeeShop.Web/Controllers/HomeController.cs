﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TheRealCoffeeShop.Web.Models;

namespace TheRealCoffeeShop.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private static HttpClient httpClient = new HttpClient();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public IActionResult OrderCoffee()
        {
            var guid = Guid.NewGuid().ToString();
            var baseURL = "https://localhost:44329/api/orders/order";

            var content = new StringContent(guid, Encoding.UTF8, "application/json");
            
            var response = httpClient.PostAsync(baseURL, content).GetAwaiter().GetResult();

            var status = response.StatusCode;
            var message = string.Empty;

            if(status == HttpStatusCode.Accepted)
            {
                message = "Your order was accepted";
            }
            else
            {
                message = "Something went wrong";
            }
            throw new NotImplementedException("Not implemented yet");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
